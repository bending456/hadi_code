# instructions for regenerating the data in our paper

## Install nanopore diffusion 
git clone https://bitbucket.org/tompace101/nanopore_diffusion/   # ? src/master/
python setup.py develop --user

Note: 
- there are dependencies associated with nanopore diffusion. 
pip3 install ruamel.yaml
fenics:
pip3 install fenics

- Also need to set the PYTHONPATH to reflect this code's location:
export PYTHONPATH=$PYTHONPATH:/u1/pkekeneshuskey/sources/git/codes/nanopore_diffusion/



git clone https://bitbucket.org/tompace101/crowder_sims/




If using the singularity image, start it with singularity run fenics_2019.simg from the directory containing the image file, and then change into this directory. (edited) 
A script is provided here to set the necessary environment variables before running. source ./set_env.sh

To generate the input data for Fig 4, run the following command (edited) 

python -m simproc requests/mk_test03.yaml

To process those data, you then run
python -m simproc requests/generated/test03.yaml
These jobs generally take up to 1 hour 


The output from test03 will be in the postproc directory 
This folder can be copied locally for data processing. 


Execution of test03 yields the file results.csv in folder test03




